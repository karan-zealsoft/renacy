<div class="crumbs">
    <ul id="breadcrumbs" class="breadcrumb">
        <li class="active">
            <i class="icon-home"></i>
            <?php
                echo $this->Html->link(
                        __('Dashboard'),
                        '/admin/admins/dashboard',
                        array(
                                'class' => 'active'
                            )
                    ); 
            ?>
        </li>
    </ul>
</div>
<div class="panel1">
    <span class="clearfix"></span>
    <div class="summary-box clearfix text-center">
            <div class="col-xs-3">
              <?php echo $this->Html->link(
                                    '<div class="red-bg"><i class="icon-user"></i>
                                        <span>0</span>
                                        <div>No. of Users</div>
                                    </div>',
                                    array(
                                            'controller' => 'admins',
                                            'action' => 'userListing',
                                            'admin' => true
                                    ),array(
                                            'escape' => false,
                                            'class' => 'remove-underline',
                                            'title' => 'New User Added'
                                    ));
                ?> 
                
            </div>
        </div>
</div>