
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Renacy</title>

    <!-- Bootstrap core CSS -->
    <?php
    	echo $this->Html->css(array(
    		'frontend/bootstrap.min',
    		'frontend/ie10-viewport-bug-workaround',
    		'frontend/style.css',
    		'frontend/sticky-footer-navbar',
    		'frontend/ie10-viewport-bug-workaround',
    		'frontend/ie10-viewport-bug-workaround',

    		)
    	)
    ?>
    
    <link href='https://fonts.googleapis.com/css?family=Lato:400,900,700,300' rel='stylesheet' type='text/css'>

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">
            <img src="img/Renacy_logo.png" class="img-responsive" alt="Renacy logo">
          </a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#pricing">Pricing</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#contact">Contact</a></li>
            <li><a href="#contact" class="btn btn-gray" data-toggle="modal" data-target="#login">Login</a></li>
            <li><a href="#contact" class="btn btn-success" data-toggle="modal" data-target="#signIn">Sign up</a></li>            
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <!-- Begin page content -->
    <div class="wrapper">
      <div class="page-header">
        <div class="jumbotron">
          <div class="container">
            <h1 class="text-uppercase">ADS BOOSTS PRODUCTION</h1>
            <p><a class="btn btn-warning btn-xlg text-uppercase" href="#" role="button">let's get started</a></p>
          </div>
      </div>

      <section class="container">
        <div class="row">
          <form>
            <div class="col-xs-6">
              <div class="form-group">
                <label class="sr-only" for="search">Search</label>
                <input type="email" class="form-control" id="search" placeholder="Search">
              </div>
            </div>
            <div class="col-xs-4">
              <div class="form-group">
                <label class="sr-only" for="exampleInputPassword3">Password</label>
                <select class="form-control">
                  <option>Category</option>
                  <option>1</option>
                  <option>1</option>
                  <option>1</option>
                </select>
              </div>
            </div>
            <div class="col-xs-2">
              <div class="form-group">
                <button type="submit" class="btn btn-gray btn-search btn-block">Search</button>
              </div>
            </div>
          </form>
        </div>
      </section>

      <section class="featured-ads container">
        <h2 class="text-center">Featured ads</h2>
        <div class="row">

          <div class="col-xs-6 col-md-3 text-center">
            <a href="#" class="thumbnail">
              <img src="img/st_patricks_day_animated_gif_21.png" alt="">
            </a>
            <div class="caption">
              <h3>Nutella</h3>
            </div>
          </div>

          <div class="col-xs-6 col-md-3 text-center">
            <a href="#" class="thumbnail">
              <img src="img/st_patricks_day_animated_gif_21.png" alt="">
            </a>
            <div class="caption">
              <h3>Nutella</h3>
            </div>
          </div>

          <div class="col-xs-6 col-md-3 text-center">
            <a href="#" class="thumbnail">
              <img src="img/st_patricks_day_animated_gif_21.png" alt="">
            </a>
            <div class="caption">
              <h3>Nutella</h3>
            </div>
          </div>

          <div class="col-xs-6 col-md-3 text-center">
            <a href="#" class="thumbnail">
              <img src="img/st_patricks_day_animated_gif_21.png" alt="">
            </a>
            <div class="caption">
              <h3>Nutella</h3>
            </div>
          </div>

          <div class="col-xs-6 col-md-3 text-center">
            <a href="#" class="thumbnail">
              <img src="img/st_patricks_day_animated_gif_21.png" alt="">
            </a>
            <div class="caption">
              <h3>Nutella</h3>
            </div>
          </div>

          <div class="col-xs-6 col-md-3 text-center">
            <a href="#" class="thumbnail">
              <img src="img/st_patricks_day_animated_gif_21.png" alt="">
            </a>
            <div class="caption">
              <h3>Nutella</h3>
            </div>
          </div>

          <div class="col-xs-6 col-md-3 text-center">
            <a href="#" class="thumbnail">
              <img src="img/st_patricks_day_animated_gif_21.png" alt="">
            </a>
            <div class="caption">
              <h3>Nutella</h3>
            </div>
          </div>

          <div class="col-xs-6 col-md-3 text-center">
            <a href="#" class="thumbnail">
              <img src="img/st_patricks_day_animated_gif_21.png" alt="">
            </a>
            <div class="caption">
              <h3>Nutella</h3>
            </div>
          </div>

        </div>
      </section>
    </div>

    <footer class="footer">
      <div class="container text-center">
        <p class="text-muted">&copy; 2007-2015 Renacy | All Right Reserved</p>
      </div>
    </footer>

    <!-- Modal -->
    <div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title" id="myModalLabel">Login</h4>
          </div>
          <div class="modal-body">
            <form>
              <div class="form-group">
                <label for="exampleInputEmail1" class="sr-only">Email address</label>
                <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1" class="sr-only">Password</label>
                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
              </div>
              <div class="checkbox">
                <label>
                  <input type="checkbox"> Keep me logged in
                </label>
              </div>
              <div class="form-group">
                <button type="submit" class="btn btn-warning btn-block">Log In</button>
              </div>
            </form>
            <p class="form-group text-right">
              <span class="text-warning">Forgot your password?</span>
            </p>
            <p class="form-group text-center">
              Don't have a Renancy Accont?<br>
              <span class="text-warning">Click here to create one</span>
            </p>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="signIn" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-circle-arrow-left text-warning"></span> &nbsp;Sign Up</h4>
          </div>
          <div class="modal-body">
            <div class="media form-group">
              <div class="media-left media-middle">
                  <span class="media-object">$50/M</span>
              </div>
              <div class="media-body media-middle">
                <h4 class="media-heading">Plus</h4>
                <p>Monthly Subscription</p>
              </div>
            </div>
            <form>
              <div class="form-group">
                <label for="firstName" class="sr-only">First Name</label>
                <input type="text" class="form-control" id="firstName" placeholder="First Name">
              </div>
              <div class="form-group">
                <label for="lastName" class="sr-only">Last Name</label>
                <input type="text" class="form-control" id="lastName" placeholder="Last Name">
              </div>
              <div class="form-group">
                <label for="email" class="sr-only">Email</label>
                <input type="email" class="form-control" id="email" placeholder="Email">
              </div>
              <div class="form-group">
                <label for="telephone" class="sr-only">Mobile</label>
                <input type="tel" class="form-control" id="telephone" placeholder="Mobile">
              </div>
              <div class="form-group">
                <label for="password" class="sr-only">Password</label>
                <input type="password" class="form-control" id="password" placeholder="Password">
              </div>
              <div class="form-group">
                <label for="rePassword" class="sr-only">Re-enter Password</label>
                <input type="password" class="form-control" id="rePassword" placeholder="Re-enter Password">
              </div>
              <div class="form-group">
                <button type="submit" class="btn btn-warning btn-block">Sign Up</button>
              </div>
            </form>
            <p class="form-group text-center">
              Already have a Renancy account?<br>
              <span class="text-warning">Click here to login</span>
            </p>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>

