<?php
	echo $this->Html->script('fckeditor');
	echo $this->Html->script('backend/AdminSettings/CmsPages/edit_cms', array('inline' => false));
?>
<div class="crumbs">
    <ul id="breadcrumbs" class="breadcrumb">
        <li class="active">
            <i class="icon-wrench"></i>
            <?php
                echo $this->Html->link(
                        __('Static CMS Pages'),
                        '/admin/admin_settings/listCmsPages',
                        array(
                                'class' => 'active',
                            )
                    ); 
            ?>
        </li>
        <li><?php echo __('Edit CMS'); ?></li>
    </ul>
</div>
<div class="panel1">    
    <div class="row">
	    <div class="col-lg-12">
	    	<?php
	    		echo $this->Form->create(
	    				'CmsPage',
	    				array(
	    					'url' => '/admin/admin_settings/editCms',
	    					'method' => 'post',
	    					'class' => 'form-horizontal',
	    					'id' => 'EditCms',
	    					'novalidate' => true
	    					)
	    			);
	    			echo $this->Form->hidden('id', array('value' => $cms['CmsPage']['id']));
	    	?>
	            <div class="row">
	                <div class="col-md-6">
	                    <div class="form-group">
	                    <label for="inputEmail3" class="col-sm-3 control-label"><?php echo __('Page Title'); ?><span>*</span></label>
	                    <div class="col-sm-9">
	                    	<?php
	                    		echo $this->Form->input('page_title', array(
	                    			'div' => false,
	                    			'label' => false,
	                    			'class' => 'form-control required',
	                    			'default' => $cms['CmsPage']['page_title']
	                    			)
	                    		);
	                    	?>
	                    </div>
	                  </div>
	                </div>
	                <div class="col-md-6">
	                    <div class="form-group">
	                    <label for="inputEmail3" class="col-sm-3 control-label"><?php echo __('Page Name'); ?><span>*</span></label>
	                    <div class="col-sm-9">
	                    	<?php
	                    		echo $this->Form->input('page_name', array(
	                    			'div' => false,
	                    			'label' => false,
	                    			'class' => 'form-control required',
	                    			'default' => $cms['CmsPage']['page_name']
	                    			)
	                    		);
	                    	?>
	                    </div>
	                  </div>
	                </div>
	            </div>
	            <div class="row">
	                <div class="col-md-6">
	                    <div class="form-group">
	                    <label for="inputEmail3" class="col-sm-3 control-label"><?php echo __('Meta Title'); ?><span>*</span></label>
	                    <div class="col-sm-9">
	                    	<?php
	                    		echo $this->Form->input('meta_title', array(
	                    			'div' => false,
	                    			'label' => false,
	                    			'class' => 'form-control required',
	                    			'default' => $cms['CmsPage']['meta_title']
	                    			)
	                    		);
	                    	?>
	                    </div>
	                  </div>
	                </div>
	                <div class="col-md-6">
	                    <div class="form-group">
	                    <label for="inputEmail3" class="col-sm-3 control-label"><?php echo __('Meta Keywords'); ?><span>*</span></label>
	                    <div class="col-sm-9">
	                    	<?php
	                    		echo $this->Form->input('meta_keyword', array(
	                    			'div' => false,
	                    			'label' => false,
	                    			'class' => 'form-control required',
	                    			'default' => $cms['CmsPage']['meta_keyword']
	                    			)
	                    		);
	                    	?>
	                    </div>
	                  </div>
	                </div>
	            </div>
		        <div class="box">
		            <header>
		                <div class="icons"><i class="icon-th-large"></i></div>
		                <h5><?php echo __('Description'); ?></h5>
		                <ul class="nav pull-right">
		                    <li>
		                        <div class="btn-group">
		                            <a class="accordion-toggle btn btn-xs minimize-box" data-toggle="collapse"
		                                href="#div-1">
		                                <i class="icon-minus"></i>
		                            </a>
		                        </div>
		                    </li>
		                </ul>
		            </header>
		            <div id="div-1" class="body collapse in">
		            	<?php
				        	echo $this->Form->input('meta_description', 
				        		array(
				        			'type' => 'textarea',
				        			'rows' => 10,
				        			'cols' => 50,
				        			'label' => false,
				        			'div' => false,
				        			'class' => '',
				        			'id' => 'MetaDescription',
				        			'default' => $cms['CmsPage']['meta_description']
			        			)
			        		);
				        	echo $this->Fck->load('MetaDescription');
				       	?>
		            </div>
		            <div class="form-actions">
		                <input type="submit" value="Submit" class="btn btn-primary btn-lg" />
		            </div>
		        </div>
		    <?php echo $this->Form->end(); ?>
	    </div>
	</div>
</div>