<?php
	echo $this->Html->script('fckeditor');
	echo $this->Html->script('backend/Admin/edit_template', array('inline' => false));
?>
<div class="crumbs">
    <ul id="breadcrumbs" class="breadcrumb">
        <li class="active">
            <i class="icon-envelope"></i>
            <?php
                echo $this->Html->link(
                        'Email Templates',
                        '/admin/email_templates/',
                        array(
                                'class' => 'active',
                            )
                    ); 
            ?>
        </li>
        <li>Edit Email Template</li>
    </ul>
</div>
<div class="panel1">    
    <div class="row">
	    <div class="col-lg-12">
	    	<?php
	    		echo $this->Form->create(
	    				'EmailTemplate',
	    				array(
	    					'url' => '/admin/email_templates/editTemplate',
	    					'method' => 'post',
	    					'class' => 'form-horizontal',
	    					'id' => 'EditEmailTemplate'
	    					)
	    			);
	    			echo $this->Form->hidden('id', array('value' => $template['EmailTemplate']['id']));
	    	?>
	            <div class="row">
	                <div class="col-md-6">
	                    <div class="form-group">
	                    <label for="inputEmail3" class="col-sm-3 control-label">Template Used For</label>
	                    <div class="col-sm-9">
	                    	<?php
	                    		echo $this->Form->input('template_used_for', array(
	                    			'div' => false,
	                    			'label' => false,
	                    			'class' => 'form-control',
	                    			'default' => $template['EmailTemplate']['template_used_for']
	                    			)
	                    		);
	                    	?>
	                    </div>
	                  </div>
	                </div>
	                <div class="col-md-6">
	                    <div class="form-group">
	                    <label for="inputEmail3" class="col-sm-3 control-label">Subject</label>
	                    <div class="col-sm-9">
		                    <?php
	                    		echo $this->Form->input('subject', array(
	                    			'div' => false,
	                    			'label' => false,
	                    			'class' => 'form-control',
	                    			'default' => $template['EmailTemplate']['subject']
	                    			)
	                    		);
	                    	?>
	                    </div>
	                  </div>
	                </div>
	            </div>
		        <div class="box">
		            <header>
		                <div class="icons"><i class="icon-th-large"></i></div>
		                <h5>Mail Body</h5>
		                <ul class="nav pull-right">
		                    <li>
		                        <div class="btn-group">
		                            <a class="accordion-toggle btn btn-xs minimize-box" data-toggle="collapse"
		                                href="#div-1">
		                                <i class="icon-minus"></i>
		                            </a>
		                        </div>
		                    </li>
		                </ul>
		            </header>
		            <div id="div-1" class="body collapse in">
		            	<?php
				        	echo $this->Form->input('mail_body', 
				        		array(
				        			'type' => 'textarea',
				        			'rows' => 10,
				        			'cols' => 50,
				        			'label' => false,
				        			'div' => false,
				        			'class' => '',
				        			'id' => 'MailBody',
				        			'default' => $template['EmailTemplate']['mail_body']
				        			)
				        		);
				        	echo $this->Fck->load('MailBody');
				       	?>
		            </div>
		            <div class="form-actions">
		                <input type="submit" value="Submit" class="btn btn-primary btn-lg" />
		            </div>
		        </div>
		    <?php echo $this->Form->end(); ?>
	    </div>
	</div>
</div>