<?php
    echo $this->element('backend/pagination_options');
?>
<table class="table table-bordered table-striped table-bordered table-hover new-addministartor-table">
    <thead>
        <tr>
            <th>
                <?php echo $this->Paginator->sort('User.first_name', 'First Name', array('class' => 'tableinherit')); ?>
            </th>
            <th>
                <?php echo $this->Paginator->sort('User.last_name', 'Last Name', array('class' => 'tableinherit')); ?>
            </th>
            <th>
                <?php echo $this->Paginator->sort('User.email', 'Email', array('class' => 'tableinherit')); ?>
            </th>
            <th><?php echo __('Status'); ?></th>
            <th><?php echo __('Actions'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php
            if(!empty($usersData)) { 
                foreach ($usersData as $userData) {
        ?>
                    <tr>
                        <td>
                            <?php echo $userData['User']['first_name']; ?>
                        </td>
                        <td>
                            <?php echo $userData['User']['last_name']; ?>
                        </td>
                        <td>
                            <?php echo $userData['User']['email']; ?>
                        </td>
                        
                        <td>
                        	<?php if ($userData['User']['is_activated'] == 0) { ?>
                                <a class="btn btn-danger btn-xs btn-grad" href="javascript:void(0);"><?php echo __('Inactive'); ?></a>
                            <?php } else { ?>
                            	<a class="btn btn-success btn-xs btn-grad" href="javascript:void(0);"><?php echo __('Active'); ?></a>
                            <?php } ?>
                        </td>
                        <td id="inline-popups">
                            <?php
								$activeInactiveAttr = $this->Admin->activeInactiveIconValues($userData['User']['is_activated'], $userData['User']['is_deleted']);
                                echo $this->Html->link(
                                        '<i class="glyphicon glyphicon-'.$activeInactiveAttr['Class'].'"></i>',
                                        $activeInactiveAttr['PopupID'],
                                        array(
                                            'class' => 'btn btn-ok activeinactivemodalbutton magniPopup',
                                            'title' => $activeInactiveAttr['Title'],
                                            'data-effect' => 'mfp-zoom-in',
                                            'escape' => false,
                                            'data-url' => '/admin/admins/changeRecordStatus/'.base64_encode($userData['User']['id']).'/User'
                                        )
                                    );
                            ?>
                            <?php
                                echo $this->Html->link(
                                        '<i class="glyphicon glyphicon-search"></i>',
                                        'javascript:void(0);',
                                        array(
                                            'class' => 'btn btn-green view_user_data',
                                            'data-effect' => 'mfp-zoom-in',
                                            'data-toggle' => 'modal',
                                            'data-target' => '#viewUserModal',
                                            'title' => 'View',
                                            'data-url' => '/admin/admins/ajaxGetFrontUserData/'.base64_encode($userData['User']['id']).'.json',
                                            'escape' => false
                                        )
                                    );
                            ?>
                            <?php
                                $deleteRestoreAttr = $this->Admin->deleteRestoreIconValues($userData['User']['is_deleted']);
                                echo $this->Html->link(
                                        '<i class="glyphicon glyphicon-'.$deleteRestoreAttr['Class'].'"></i>',
                                        $deleteRestoreAttr['PopupID'],
                                        array(
                                            'class' => 'btn btn-red deleteModalButton magniPopup',
                                            'title' => $deleteRestoreAttr['Title'],
                                            'data-effect' => 'mfp-zoom-in',
                                            'data-url' => '/admin/admins/delete/'.base64_encode($userData['User']['id']).'/User',
                                            'escape' => false
                                        )
                                    );
                            ?>
                        </td>
                    </tr>
        <?php } } else { ?>
            <tr>
                <td colspan="6">
                    <?php echo __('No Record Found'); ?>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<?php
    echo $this->element('backend/pagination');
    echo $this->Js->writeBuffer();
?>