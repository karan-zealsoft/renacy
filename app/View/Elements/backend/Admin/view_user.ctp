<div class="modal fade" id="viewUserModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog user-view-popup">
        <div class="modal-content">
            <div class="modal-header">
                <span class="button b-close class="close" data-dismiss="modal" aria-label="Close"">
                     <span>X</span>
                </span>
                <h4 class="modal-title" id="myModalLabel"><?php echo __('View/Edit User Details'); ?></h4>
            </div>
            <div class="modal-body">

                <?php echo $this->Form->create('Admins', array(
                                'action' => 'saveDetails',
                                'method' => 'post',
                                'admin' => true,
                                'id' => 'view-user',
                                'enctype' => "multipart/form-data"
                                ));
                ?>
                    <div id="userDataContainer">
                    </div>
                    <div class="form-group">
                        <div class="clearfix">                               
                            <div class="pull-right">
                                <?php
                                     echo $this->Form->button('Save', array(
                                                'type' => 'submit',
                                                'class' => 'btn btn-primary',
                                                
                                                ));?>
                                <?php
                                    echo $this->Form->button('Close', array(
                                                'class' => 'btn btn-default',
                                                'data-dismiss' => 'modal'
                                                ));
                                ?>
                            </div>
                        </div>
                    </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>

<script type="text/html" id="viewUserField">
<div class="form-group">
        <div class="col-xs-12 form-group clearfix">
            <div class="col-xs-4">
                <label class="control-label">Click on Edit</label>
            </div>

            <div class="col-xs-8 clearfix">
                <a class="editUser" href="javascript:void(0)">Edit</a>
           </div>
        </div>
        <div class="col-xs-12 form-group clearfix">
            <div class="col-xs-4">
                <label class="control-label">First Name</label>
            </div>

            <div class="col-xs-8 clearfix">
            <input type="hidden"  class="form-control"  name="data[User][id]" value="<%= data.User.id %>"> 
            <input type="hidden"  class="form-control"  name="data[User][user_id]" value="<%= data.User.user_id %>"> 
            <input type="text" readonly='readonly' class="form-control"  name="data[User][first_name]" value="<%= data.User.first_name %>">     
           </div>
        </div>
        <div class="col-xs-12 form-group clearfix">
            <div class="col-xs-4">
                <label class="control-label">Last Name</label>
            </div>

            <div class="col-xs-8 clearfix">
                <input type="text" readonly='readonly' class="form-control"  name="data[User][last_name]" value="<%= data.User.last_name %>"> 
           </div>
        </div>
        <div class="col-xs-12 form-group clearfix">
            <div class="col-xs-4">
                <label class="control-label">UserName</label>
            </div>

            <div class="col-xs-8 clearfix">
                <input type="text" readonly='readonly' class="form-control"  name="data[User][username]" value="<%= data.User.username %>"> 
                
           </div>
        </div>
        
        <div class="col-xs-12 form-group clearfix">
            <div class="col-xs-4">
                <label class="control-label">Email</label>
            </div>

            <div class="col-xs-8 clearfix">
                <input type="text" readonly='readonly'  class="form-control"  name="data[User][email]" value="<%= data.User.email %>"> 
               
           </div>
        </div>
        

  
        <div class="col-xs-12 form-group clearfix">
            <div class="col-xs-4">
                <label class="control-label">Mobile</label>
            </div>

            <div class="col-xs-8 clearfix">
                <input type="text" readonly='readonly'  class="form-control" required="required"  name="data[User][mobile]" value="<%= data.User.mobile %>"> 
           </div>
        </div>
    </div>
</script>
