<div id="inactiveConfirm" class="white-popup mfp-with-anim mfp-hide">
    <h3 class="text-center popup-heading">Are you sure want to deactivate?</h3>
    <div CLASS="text-center"> 
        <button class="btn btn-default close-btn" data-dismiss="modal" type="button">No</button>
        <?php
            echo $this->Html->link(
                    'Yes',
                    '#',
                    array(
                        'class' => 'btn btn-primary btn-grad sureActiveInactive',
                        'escape' => false
                        )
                );
        ?>
    </div> 
</div>