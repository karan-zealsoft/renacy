<?php
	echo $this->element('backend/pagination_options');
?>
<table class="table table-bordered table-striped table-bordered table-hover new-addministartor-table">
	<thead>
		<tr>
			<th>
				<?php echo $this->Paginator->sort('Category.title', 'Category Name', array('class' => 'tableinherit')); ?>
			</th>
			<?php if(!empty($parentId)):?>
				<th>
					<?php echo __('Parent Category'); ?>
				</th>
			<?php  endif;?>

			<th>
				<?php echo $this->Paginator->sort('Category.title', 'Status', array('class' => 'tableinherit')); ?>
			</th>
			
			<th><?php echo __('Actions'); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php

			if(!empty($categories)) { 

				foreach ($categories as $category) {
		?>
					<tr>
						<td>
							<?php echo $category['Category']['title']; ?>
						</td>
						<?php if(!empty($parentId)):?>
							<td>
								<?php echo $parentTitle; ?>
							</td>
						<?php  endif;?>
						<td>
							<?php if ($category['Category']['is_activated'] == 0) { ?>
								<a class="btn btn-danger btn-xs btn-grad" href="#"><?php echo __('Inactive'); ?></a>
							<?php } else { ?>
								<a class="btn btn-success btn-xs btn-grad" href="#"><?php echo __('Active'); ?></a>
							<?php } ?>
						</td>
						<td id="inline-popups">
							<?php
								if ($category['Category']['is_activated'] == 1) :

									echo $this->Html->link(
											'<i class="glyphicon glyphicon-search"></i>',
											'/admin/Categories/index/'.base64_encode(convert_uuencode($category['Category']['id'])),
											array(
												'class' => 'btn btn-green edit_category_data',
												'data-effect' => 'mfp-zoom-in',
												'data-toggle' => 'modal',
												'title' => 'View '.$category['Category']['title'].' Sub Categories',
												'escape' => false
											)
										);
								endif;

								$activeInactiveAttr = $this->Admin->activeInactiveIconValues($category['Category']['is_activated'], $category['Category']['is_deleted']);
								echo $this->Html->link(
										'<i class="glyphicon glyphicon-'.$activeInactiveAttr['Class'].'"></i>',
										$activeInactiveAttr['PopupID'],
										array(
											'class' => 'btn btn-ok activeinactivemodalbutton magniPopup',
											'title' => $activeInactiveAttr['Title'],
											'data-effect' => 'mfp-zoom-in',
											'escape' => false,
											'data-url' => '/admin/admins/changeRecordStatus/'.base64_encode($category['Category']['id']).'/Category'
										)
									);
						   
								echo $this->Html->link(
										'<i class="glyphicon glyphicon-pencil"></i>',
										'javascript:void(0)',
										array(
											'class' => 'btn btn-green edit_category_data',
											'data-effect' => 'mfp-zoom-in',
											'data-toggle' => 'modal',
											'data-target' => '#editCategoryModal',
											'title' => 'Edit',
											'data-url' => '/admin/Categories/getCategoryData/'.base64_encode($category['Category']['id']),
											'escape' => false
										)
									);
							
								$deleteRestoreAttr = $this->Admin->deleteRestoreIconValues($category['Category']['is_deleted']);
								echo $this->Html->link(
										'<i class="glyphicon glyphicon-'.$deleteRestoreAttr['Class'].'"></i>',
										$deleteRestoreAttr['PopupID'],
										array(
											'class' => 'btn btn-red deleteModalButton magniPopup',
											'title' => $deleteRestoreAttr['Title'],
											'data-effect' => 'mfp-zoom-in',
											'data-url' => '/admin/Categories/delete/'.base64_encode($category['Category']['id']).'/Category',
											'escape' => false
										)
									);
							?>
						</td>
					</tr>
		<?php } } else { ?>
			<tr>
				<td colspan="8">
					<?php echo __('No Record Found'); ?>
				</td>
			</tr>
		<?php } ?>
	</tbody> 
</table>
<?php
	echo $this->element('backend/pagination');
	echo $this->Js->writeBuffer();
?>