<div class="modal fade" id="editCategoryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <span class="button b-close class="close" data-dismiss="modal" aria-label="Close"">
                     <span>X</span>
                </span>
                <h4 class="modal-title" id="myModalLabel">Edit Category</h4>
            </div>
            <div class="modal-body">
                <?php 
                    echo $this->Form->create('Category', array(
                                'url' => array(
                                    'controller' => 'Categories',
                                    'action' => 'edit',
                                    'admin' => true
                                ),
                                'id' => 'edit-category',
                                'method' => 'post'
                            )
                        );
                ?>
                    <div id="categoryDataContainer">
                    
                    </div>
                    <div class="form-group">
                        <div class="clearfix">                               
                            <div class="pull-right">
                                <?php
                                    echo $this->Form->button('Close', array(
                                                'class' => 'btn btn-default',
                                                'data-dismiss' => 'modal'
                                                ));
                                    echo $this->Form->button('Save', array(
                                                'class' => 'btn btn-primary btn-grad btn-left',
                                                'type' => 'submit'
                                                ));
                                ?>
                            </div>
                        </div>
                    </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>

<script type="text/html" id="editCategoryeData">
    <input type='hidden' name='data[Category][id]' value="<%= data.Category.id %>" />
	<div class="form-group">
		<label class="control-label">First Name<span>*</span></label>
    	<input type='text' class='form-control' required placeholder='First Name' value="<%= data.Category.title %>" name='data[Category][title]' />
	</div>
	
</script>