<?php

App::uses('AppHelper', 'View/Helper');

/**
 * Application helper
 *
 * Add your application-wide methods in the class below, your helpers
 * will inherit them.
 *
 * @package       app.View.Helper
 */
class UserDataHelper extends AppHelper {

/**
 * Method userProfilePicturePath to get the path for user profile picture
 *
 * @return string $profilePic contains the path of the user profile picture
 */
    public function userProfilePicturePath($userPic = null) {
    	if (!filter_var($userPic, FILTER_VALIDATE_URL) === false) {
    		return $userPic;
    	}

        if (!file_exists(WWW_ROOT . substr(Configure::read('UserImagePath'), 1) . $userPic)) {
            return Configure::read('UserDummyImagePath');
        }

        $profilePic = !empty($userPic) ? Configure::read('UserImagePath').$userPic : Configure::read('UserDummyImagePath');
        return $profilePic;
    }

/**
 * Method leftNavTabActive to get active class for current opened left navigation tab
 *
 * @param string $controller name of the controller
 * @param string $action name of the action
 * @return string $activeClass the active class or empty
 */
    public function leftNavTabActive($controller = null, $action = null) {
    	$activeClass = ($this->params->params['controller'] == $controller && $this->params->params['action'] == $action) ? 'active' : '';
        return $activeClass;
    }

/**
 * Method highLightNumber to get matched numbers
 *
 * @param string $controller name of the controller
 * @param string $action name of the action
 * @return string $activeClass the active class or empty
 */
    public function highLightNumber($controller = null, $action = null) {
        $activeClass = ($this->params->params['controller'] == $controller && $this->params->params['action'] == $action) ? 'active' : '';
        return $activeClass;
    }

}
