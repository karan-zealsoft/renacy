<?php
class AdminSettingsController extends AppController
{
	public $helper = array('Html', 'Form');
	public $components = array('Paginator', 'RequestHandler');

	public $uses = array('CmsPage');

	public function beforeFilter() {
		parent::beforeFilter();
	}

/**
 * Method admin_index to display all created spaces
 *
 * @return void 
 */
	public function admin_index() {

	}


/**
 * Method admin_listCmsPages to view all the cms pages of the website
 *
 * @return void 
 */
	public function admin_listCmsPages() {
		$this->layout = 'backend';
		$this->loadModel('CmsPage');
		$getAllCmsPages = $this->CmsPage->find('all');
		$this->set('getAllCmsPages', $getAllCmsPages);
	}

/**
 * Method admin_editCms to save upadted records
 *
 * @return void 
 */
	public function admin_editCms($cmsID=null) {
		$this->loadModel('CmsPage');
		if(isset($this->request->data) && !empty($this->request->data)) {
			if ($this->CmsPage->save($this->request->data)) {
				$this->Session->setFlash(__('Cms page updated successfully.'), 'default', 'success');
				$this->redirect(array('action' => 'listCmsPages', 'admin' => true));
			} else {
				$errors = $this->CmsPage->validationErrors;
	            if (!empty($errors)) {
	                $errorMsg = $this->_setValidaiotnError($errors);
	            }
				$this->Session->setFlash(__('Cms page update request not completed due to following errors: <br/>' . $errorMsg . ' Try again!'), 'default', 'error');
			}
			$this->redirect($this->referer());
		}
		$this->layout = 'backend';
		$cmsID = base64_decode($cmsID);
		$cms = $this->CmsPage->findById($cmsID, array(
			'id','meta_title', 'meta_description', 'meta_keyword', 'page_name', 'page_title'));
		//pr($cms);die;
		$this->set('cms',$cms);
	}

}
