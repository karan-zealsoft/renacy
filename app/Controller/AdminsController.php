<?php
// App::uses('User', 'View/Helper'); 
class AdminsController extends AppController
{
	public $helper = array('Html', 'Form', 'Js', 'Admin', 'UserData');
	public $components = array('RequestHandler', 'Paginator');
	public $uses = array('User','UserPlay');
	public $layout = "backend";

	public function beforeFilter() {
		parent::beforeFilter();
		$this->_loginWithCookie();
	}

/**
 * Method admin_index get the list of all sub admins
 *
 * @return void 
 */
	public function admin_index() {
		
		if (!$this->_checkIfAdminOrUser()) {
			$this->redirect(array('action' => 'dashboard'));
		}
		$conditions = array(
			'User.user_type_id' => configure::read('UserTypes.SubAdmin')
		);
		if (isset($this->request->query) && !empty($this->request->query)) {
			$searchData = array(
				'OR' => array(
					'User.first_name LIKE' => '%'. $this->request->query['search'] .'%',
					'User.last_name LIKE' => '%'. $this->request->query['search'] .'%',
					'User.email LIKE' => '%'. $this->request->query['search'] .'%',
					'User.mobile LIKE' => '%'. $this->request->query['search'] .'%',
					)
				);
			$conditions = array_merge($conditions, $searchData);
		}
		
		$this->Paginator->settings = array(
			'conditions' => $conditions,
			'limit' => 10,
			'order' => 'User.created Desc',
		);
		$subAdmins = $this->Paginator->paginate('User');
		$this->set('subAdmins',$subAdmins);
		if ($this->request->is('ajax')) {
			$this->layout = '';
			$this->autoRender = false;
			$this->viewPath = 'Elements' . DS . 'backend' . DS . 'Admin';
			$this->render('listing');
		}
	}

/**
 * Method _loginWithCookie to login the user with cookie
 *
 * @return void
 */
	protected function _loginWithCookie() {
		if (!$this->Auth->loggedIn() && $this->Cookie->read('User')) {
        	$this->loadModel('User');
		    $user = $this->User->find('first', array(
				'conditions' => array(
				    'User.email' => $this->Cookie->read('User.email'),
				    'User.persist_code' => $this->Cookie->read('User.persist_code'),
				    'User.is_deleted' => (int)false,
				    'User.is_activated' => (int)true
				)
		    ));

		    if ($user && !$this->Auth->login($user['User']) || empty($user)) {
				return $this->redirect($this->Auth->logout());
		    } 

		    return $this->redirect($this->Auth->redirectUrl());
		}
	}

/**
 * Method admin_login
 *
 * @return void
 */
	public function admin_login() {
		$this->layout = 'backend_login';
		$this->set('titleForLayout', 'Renacy | Login');

		if ($this->Auth->loggedIn()) {

			$this->__loggedIn();
		}

		if ($this->request->is('post')) {
			if ($this->Auth->login()) {
				$userTypes = array(
					Configure::read('UserTypes.Admin'),
					Configure::read('UserTypes.SubAdmin')
				);
				if (!$this->_checkUserType($userTypes)) { // check if logged in user is not a front end user
					$this->Session->setFlash(
							__('you are not authorized to access this location.'),
							'default',
							'error'
						);
					return $this->redirect($this->Auth->logout());
				}

				$redirectUrl = !empty($this->Session->read('Auth.redirect')) ? $this->Session->read('Auth.redirect') : null;
				
				if ($this->data['User']['keep_me_logged_in'] == true) {
					$cookie = array(
							'email' => $this->Session->read('Auth.User.email'),
							'persist_code' => $this->Session->read('Auth.User.persist_code')
					);
					$this->Cookie->httpOnly = true;
					$this->Cookie->write('User', $cookie, true, '7 Days');
				}
			
				if (!empty($redirectUrl)) {
					$this->redirect($redirectUrl);
				}

				$this->redirect(array('controller' => 'admins', 'action' => 'index', 'admin' => true));
			}

			$this->Session->setFlash(
				__('Invalid Username or Password'),
				'default',
				'error'
			);
			$this->redirect($this->referer());
		}
	}

/**
 * Method __loggedIn to  redirct user dashboard
 *
 * @return void
 */
	private function __loggedIn() {
		
		if (in_array($this->Session->read('Auth.User.user_type_id') ,array(Configure::read('UserTypes.Admin'),
					Configure::read('UserTypes.SubAdmin')))) {
			$this->redirect(array('controller' => 'admins', 'action' => 'index', 'admin' => true));
		}

		$this->redirect(array('controller' => 'homes', 'action' => 'index'));
	}


/**
 * Method admin_logout for admin logout
 *
 * @return void
 */
	public function admin_logout() {
		$this->Cookie->delete('User');
		$this->Session->delete('Auth');
		return $this->redirect($this->Auth->logout());
	}
	
/**
 * Method admin_change_password for change admin password
 *
 * @return void
 */
	public function admin_change_password() {
		$this->request->allowMethod('post','put');
		$this->_changePassword();
	}
	
/**
 * Method admin_getUserData get data to show in the edit profile popup
 *
 * @return void
 */
	public function admin_ajaxGetUserData($userId=null) {
		
		$userId = base64_decode($userId);
		
		$getUserData = $this->User->find('first',array(
				'conditions' => array(
					'User.id' => (int)$userId 
				),
			)
		);

		$this->set(array('resp' => $getUserData, '_serialize' => 'resp'));
	}
	
/**
 * Method admin_edit_profile for edit admin profile
 *
 * @return void
 */
	public function admin_edit_profile() {
		$this->request->allowMethod('post','put');

		$this->User->unvalidate(array('username', 'old_password','password','confirm_password'));
		if ($this->User->save($this->request->data)) {
			$this->Session->setFlash(__('Profile updated successfully.'), 'default', 'success');
			$this->redirect($this->referer());
		}
		$errors = $this->User->validationErrors;
		if (!empty($errors)) {
			$errorMsg = $this->_setSaveAssociateValidationError($errors);
		}
		$this->Session->setFlash(__('Edit profile request not completed due to following : <br/>' . $errorMsg . ' Try again!'), 'default', 'error');
		$this->redirect($this->referer());
	}
	
/**
 * Method admin_forgotPassword to send temporary password to user's email
 *
 * @return void
 */
	public function admin_forgotPassword() {
		$this->request->allowMethod('post','put');
		$getAdminDetail = $this->User->find('first', array(
           'conditions' => array(
           		'OR' => array(
               			'User.username' => $this->request->data['User']['username'],
               			'User.email' => $this->request->data['User']['username'],
           		),
           		'User.is_activated' => (int)true,
           		'User.user_type_id' => array(Configure::read('UserTypes.Admin'),Configure::read('UserTypes.SubAdmin'))
               )
           )
		);
		
		if (empty($getAdminDetail)) {
			$this->Session->setFlash(__('you are not authorized or blocked by administrator.'), 'default', 'error');
			$this->redirect($this->referer());
		}

		$link = FULL_BASE_URL.'/admin/admins/resetPassword/' .urlencode(base64_encode($getAdminDetail['User']['persist_code']));
		
		if (!$this->__sendForgotPasswordEmail($getAdminDetail, $link)) {
			$this->Session->setFlash(__('Some error occurred. Please try again'), 'default', 'error');
		}
		
		$this->Session->setFlash(__('Please check your mailbox, An instruction has been sent to reset your password.'), 'default', 'success');
		$this->redirect($this->referer());
	}

/**
 * Method _sendForgotPasswordEmail to get email template and send email with temporary password
 *
 * @return void
 */
	private function __sendForgotPasswordEmail($user, $link) {
		$this->loadModel('EmailTemplate');
		$temp = $this->EmailTemplate->findById(2);
      	$temp['EmailTemplate']['mail_body'] = str_replace(
            array('#NAME','#EMAIL','#USERNAME', '#CLICKHERE'),
            array($user['User']['first_name']. ' ' .$user['User']['last_name'],$user['User']['email'],$user['User']['username'],$link), $temp['EmailTemplate']['mail_body']);

  	  	return $this->_sendEmailMessage($user['User']['email'], $temp['EmailTemplate']['mail_body'], $temp['EmailTemplate']['subject']);
	}
	
/**
 * Method admin_resetPassword to reset admin user password
 *
 * @return void
 */
	public function admin_resetPassword($persistCode = null) {
		
		if ($this->request->is('post')) {
			$this->User->unvalidate(array('username', 'email', 'old_password'));
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('Your Password has been changed successfully. You can login to continue'), 'default', 'success');
				$this->redirect(array('action' => 'login', 'admin' => true));
			} 
			$errors = $this->User->validationErrors;
			if (!empty($errors)) {
				$errorMsg = $this->_setValidaiotnError($errors);
				$this->Session->setFlash(__('Password reset request not completed due to following error : <br/>' . $errorMsg . ' Try again!'), 'default', 'error');
				$this->redirect($this->referer());
			}
		}

		$this->layout = 'backend_login';
		$persistCodeEnc = urldecode(base64_decode($persistCode));
		$getUserId = $this->User->findByPersistCode($persistCodeEnc, array('id'));
		if (empty($getUserId)) {
			$this->redirect(array('action' => 'login', 'admin' => true));
		}

		$this->set('userID',$getUserId['User']['id']);
	}
	
/**
 * Method admin_add_subadmin for add new sub admin
 *
 * @return void
 */
	public function admin_addSubadmin() {
		$this->request->allowMethod('post','put');

		$this->User->unvalidate(array('old_password'));
		$this->request->data['User']['user_type_id'] = configure::read('UserTypes.SubAdmin');

		app::uses('String','Utility');
		$this->request->data['User']['activation_key'] = String::uuid();

		App::uses('CakeTime', 'Utility');
		$this->request->data['User']['persist_code'] = CakeTime::fromString(date('Y-m-d'));

		if ($this->User->save($this->request->data)) {
			//$this->admin_addSubAdminEmail();
			$this->Session->setFlash('Sub-Admin added successfully.', 'default', 'success');
		} else {
			$errors = $this->User->validationErrors;
			if (!empty($errors)) {
				$errorMsg = $this->_setSaveAssociateValidationError($errors);
			}
			$this->Session->setFlash('Sub-Admin add request not completed due to following errors: <br/>' . $errorMsg . ' Try again!', 'default', 'error');
		}
		$this->redirect(array('action' => 'index', 'admin' => true));
	}

/**
 * Method admin_addSubAdminEmail for send registration notification to new sub admin added by admin itself
 *
 * @return void
 */
	private function admin_addSubAdminEmail() {
	
		$this->loadModel('EmailTemplate');

		$link = "<a href=" . Configure::read('ROOTURL').'admin/admins/subAdminActivateAccount/' .$this->request->data['User']['activation_key']. ">Click Here </a> to activate your account.";

	   $temp = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.id' => Configure::read('EmailTemplateId.registration_notification'))));
	   $temp['EmailTemplate']['mail_body'] = str_replace(
			array('../../..', '#NAME', '#EMAIL', '#PASSWORD', '#CLICKHERE'),
			array(FULL_BASE_URL, $this->request->data['UserProfile']['first_name'].' '.$this->request->data['UserProfile']['last_name'], $this->request->data['User']['email'], $this->request->data['UserProfile']['confirm_password'], $link), $temp['EmailTemplate']['mail_body']);
		
		return $this->_sendEmailMessage($this->request->data['User']['email'], $temp['EmailTemplate']['mail_body'], $temp['EmailTemplate']['subject']);
		
	}


/**
 * Method admin_addSubAdminEmail for send registration notification to new sub admin added by admin itself
 *
 * @return void
 */
	public function admin_saveDetails() {
		
		$this->request->allowMethod('post','put');
		$this->User->id = $this->request->data['User']['id']; 
		$this->User->unvalidate(array('old_password','password', 'confirm_password'));
		if ($this->User->save($this->request->data)) {
			$this->Session->setFlash('User Details Edited successfully.', 'default', 'success');
			
		} else {
			$errors = $this->User->validationErrors;
			
			if (!empty($errors)) {
				$errorMsg = $this->_setSaveAssociateValidationError($errors);
			}
			$this->Session->setFlash('User detail edit request not completed due to following errors: <br/>' . $errorMsg . ' Try again!', 'default', 'error');
		}
		$this->redirect(array('action' => 'userListing', 'admin' => true));
	}
/**
 * Method _updateUserForRemovePicture common  function to run query for update user table so that when user wants to remove profile pic only one query can execute
 *
 * @return bool
 */
	private function __updateUserForRemovePicture($id) {
		
		$this->loadModel('UserProfile');
		if (
			$this->UserProfile->updateAll(
				array(
						'UserProfile.profile_pic' => null
					),
				array(
						'UserProfile.user_id' => $id
					)
			)
			) {
				
			
				return true;
		}
		return false;
	}


/**
 * Method __removePicture to remove user profile picture
 *
 * @return void
 */
	public function admin_removeUserImage($id = null) {

		$userInfo = $this->UserProfile->find('first',array('conditions'=> array('UserProfile.user_id' => $id)));
		if (trim($userInfo['UserProfile']['profile_pic']) != null) {

			if (!filter_var($userInfo['UserProfile']['profile_pic'], FILTER_VALIDATE_URL) === false) {
				$this->__updateUserForRemovePicture($id);

			}
			if (file_exists(WWW_ROOT . substr(Configure::read('UserImagePath'), 1) . $userInfo['UserProfile']['profile_pic'])) {
				unlink(WWW_ROOT . substr(Configure::read('UserImagePath'), 1) . $userInfo['UserProfile']['profile_pic']);
				$this->__updateUserForRemovePicture($id);
			} else {
				$this->__updateUserForRemovePicture($id);
			}
			$this->Session->setFlash(__('Profile picture has been removed successfully.'), 'default', 'success');
		} else {
			$this->Session->setFlash(__('Nothing to remove.'), 'default', 'error');
		}
		
		$this->redirect($this->referer());
	}


/**
 * Method admin_subAdminActivateAccount to activate sub admins accounts
 *
 * @param $activationKey activation key of sub admin
 * @return void
 */
	public function admin_subAdminActivateAccount($activationKey = null) {
		$checkRecord = $this->User->findByActivationKeyAndIsActivated($activationKey, '0', array('id', 'is_activated'));
		if (empty($checkRecord)) {
			$this->Session->setFlash('This link has been expired!', 'default', 'error');
			$this->redirect(array('action' => 'login', 'admin' => true));
		}
		if($this->User->activateAccount($activationKey)) {
			$this->Session->setFlash(__('Your account has been activated successfully! Please login to continue.'), 'default', 'success');
		} else {
			$this->Session->setFlash(__('Some errors occurred. Please try again.'), 'default', 'error');
		}
		$this->redirect(array('action' => 'login', 'admin' => true));
	}
	
/**
 * Method admin_changeSubAdminStatus to activate inactivate the sub admin status
 *
 * @param $userID int the user id of the sub admin
 * @return void
 */
	public function admin_changeRecordStatus($recordId = null, $model = null, $module = null) {
		$recordId = base64_decode($recordId);
		$this->loadModel($model);
		$checkRecord = $this->$model->findById($recordId, array('id', 'is_activated'));

		if (empty($checkRecord)) {
			$this->Session->setFlash(__('This Record does not exist.'), 'default', 'error');
			$this->redirect($this->referrer());
		}

		$status = $this->_changeAccountStatus($checkRecord,$model);
		$moduleName = empty($module) ? $model : $module;
		$this->Session->setFlash(__($moduleName.' has been activated successfully.'), 'default', 'success');
		if ($status == (int)false) {
			$this->Session->setFlash(__($moduleName.' has been deactivated successfully.'), 'default', 'success');
		}
		$this->redirect($this->referer());
	}
	
/**
 * Method admin_deletesubAdmin to delete sub admin
 *
 * @param $userID int the user id of the sub admin
 * @return void
 */
	public function admin_delete($userID = null, $model = null) {
		$userID = base64_decode($userID);
		$this->loadModel($model);
		$checkRecord = $this->$model->findById($userID, array('id', 'is_deleted'));
		if (empty($checkRecord)) {
			$this->Session->setFlash('Record does not exist.', 'default', 'error');
			$this->redirect($this->referrer());
		}

		$status = $this->_deleteAccount($checkRecord[$model]['id'], $checkRecord[$model]['is_deleted'], $model);
		$moduleName = empty($module) ? $model : $module;
		$this->Session->setFlash(__($moduleName.' has been deleted successfully.'), 'default', 'success');
		if ($status == (int)false) {
			$this->Session->setFlash(__($moduleName.' has been restored successfully.'), 'default', 'success');
		}
		$this->redirect($this->referer());
	}

/**
 * Method admin_dashboard
 */
	public function admin_dashboard() {
		$activeUsers = $this->User->activeUserCount();
		$this->set(compact('activeUsers'));		
	}


/**
 * Method admin_changeRecordStatusByActive to activate inactivate the sub admin status
 *
 * @param $recordId int id of the table
 * @param $model string the name of the model
 * @param $$module string name of the module to show in seccess messages
 * @return void
 */
	public function admin_changeRecordStatusByActive($recordId = null, $model = null, $module = null) {
		
		$recordId = base64_decode($recordId);
		$this->loadModel($model);
		$checkRecord = $this->$model->findById($recordId, array('id', 'is_active'));
		pr($checkRecord); die;
		if (empty($checkRecord)) {
			$this->Session->setFlash(__('This Record does not exist.'), 'default', 'error');
			$this->redirect($this->referrer());
		}

		$status = $this->_changeAccountStatusByActive($checkRecord,$model);
		$moduleName = empty($module) ? $model : $module;
		$this->Session->setFlash(__($moduleName.' has been activated successfully.'), 'default', 'success');
		if ($status == (int)false) {
			$this->Session->setFlash(__($moduleName.' has been deactivated successfully.'), 'default', 'success');
		}
		$this->redirect($this->referer());
	}

	public function admin_userListing() {
		$this->layout = 'backend';
		$conditions = array(
			'User.user_type_id' => configure::read('UserTypes.User'),
			);
		if (isset($this->request->query) && !empty($this->request->query)) {
			$searchData = array(
				'OR' => array(
						'User.first_name LIKE' => '%'. $this->request->query['search'] .'%',
						'User.last_name LIKE' => '%'. $this->request->query['search'] .'%',
						'User.email LIKE' => '%'. $this->request->query['search'] .'%',
					)
				);
			$conditions = array_merge($conditions, $searchData);
		}
		
		$this->Paginator->settings = array(
										'conditions' => $conditions,
										'limit' => 10,
										'order' => 'User.created Desc',
										//'fields' => array('id', 'email', 'is_activated', 'is_deleted', 'social_network_user'),
										
									);
		$usersData = $this->Paginator->paginate('User');
		$this->set('usersData',$usersData);
		if ($this->request->is('ajax')) {
			$this->layout = '';
			$this->autoRender = false;
			$this->viewPath = 'Elements' . DS . 'backend' . DS . 'Admin';
			$this->render('userlisting');
		}
	}

	public function admin_ajaxGetFrontUserData($user_id = null) {
		$user_id = urldecode(base64_decode($user_id));
		$fields = array(
				'User.*',
				
			);
		$getUserData = $this->User->find('first',array(
				'conditions' => array(
					'User.id' => $user_id,
				),
				'fields' => $fields,
				'recursive' => -1
				)
		);
		// $img_with_path = $this->__userProfilePicturePath($getUserData['User']['profile_pic']);
		// unset($getUserData['User']['profile_pic']);
		// $getUserData['User']['profile_pic'] = $img_with_path;
		$this->set(
				array(
					'response' => $getUserData,
					'_serialize' => 'response'
				)
			);
	}

	public function admin_addFrontUser() {
		$this->request->allowMethod('post','put');

		$this->User->unvalidate(array('old_password'));
		$this->request->data['User']['user_type_id'] = configure::read('UserTypes.User');
		$this->request->data['User']['is_activated'] = 1;

		app::uses('String','Utility');
		$this->request->data['User']['activation_key'] = String::uuid();

		App::uses('CakeTime', 'Utility');
		$this->request->data['User']['persist_code'] = CakeTime::fromString(date('Y-m-d'));

		if ($this->User->save($this->request->data)) {
			$this->Session->setFlash('User added successfully.', 'default', 'success');
		} else {
			$errors = $this->User->validationErrors;
			if (!empty($errors)) {
				$errorMsg = $this->_setSaveAssociateValidationError($errors);
			}
			$this->Session->setFlash('User adding request not completed due to following errors: <br/>' . $errorMsg . ' Try again!', 'default', 'error');
		}
		$this->redirect(array('action' => 'userListing', 'admin' => true));
	}

	private function __userProfilePicturePath($userPic = null) {
		if (!filter_var($userPic, FILTER_VALIDATE_URL) === false) {
			return $userPic;
		}

		if (!file_exists(WWW_ROOT . substr(Configure::read('UserImagePath'), 1) . $userPic)) {
			return Configure::read('UserDummyImagePath');
		}

		$profilePic = !empty($userPic) ? Configure::read('UserImagePath').$userPic : Configure::read('UserDummyImagePath');
		return $profilePic;
	}

}
