<?php
class CategoriesController extends AppController
{
	public $helper = array('Html', 'Form', 'Js', 'Admin', 'UserData');
	public $components = array('RequestHandler', 'Paginator');
	public $uses = array('Category');
	public $layout = 'backend';	

	public function beforeFilter() {
		parent::beforeFilter();
	}

/**
 * Method admin_index to display all categories
 *
 * @return void 
 */
	public function admin_index($parentId = null) {
		$parentTitle = null;
		$conditions = array();
		
		if (isset($this->request->query) && !empty($this->request->query)) {
			
			if (!empty($this->request->query['parent_id']))  {
				$parentId = base64_encode(convert_uuencode($this->request->query['parent_id']));	
			}

			$searchData = array(
				'OR' => array(
					'Category.title LIKE' => '%'. $this->request->query['search'] .'%',
					),
					'Category.parent_id' => $this->request->query['parent_id'],
				);
			$conditions = array_merge($conditions, $searchData);
		}



		if (!empty($parentId)) {
			$parentId = convert_uudecode(base64_decode($parentId));
			$title = $this->Category->findById($parentId, array('id','title'));
			$parentTitle = $title['Category']['title'];
		}

		$conditions[] = array(
			'Category.parent_id' => $parentId,
		);
		$treePath = $this->Category->getPath($parentId);
		$categories = $this->_paginate($conditions);
		$this->set(compact('categories', 'parentId', 'parentTitle', 'treePath'));
		
		if ($this->request->is('ajax')) {
			$this->layout = '';
			$this->autoRender = false;
			$this->viewPath = 'Elements' . DS . 'backend' . DS . 'Category';
			$this->render('listing');
		}
	}

	protected function _paginate($params) {
		
		$this->Paginator->settings = array(
			'conditions' => $params,
			'limit' => 10,
			'order' => 'Category.modified DESC'
		);
		return $this->Paginator->paginate();
	}

	public function admin_add() {
		$this->request->allowMethod('post');
		$data = $this->request->data;
		
		if (!empty($data['Category']['parent_id'])) {

			$existsData = $this->_existsParentId($data['Category']['parent_id']);
			if (!$existsData) {
				$this->Session->setFlash('Please provide valid parent category', 'default', 'error');
				$this->redirect($this->referer());	
			}
		}
		
		if(!empty($data)) {
			$categoryData['Category'] = array(
				'parent_id' => $data['Category']['parent_id'],
				'title' => $data['Category']['title']
			);

			if (!$this->Category->save($categoryData)) {
				$errors = $this->Category->validationErrors;
				$errorMsg = $this->_setSaveAssociateValidationError($errors);
				$this->Session->setFlash('Category add request not completed due to following errors: <br/>' . $errorMsg . ' Try again!', 'default', 'error');
				$this->redirect($this->referer());
			}

			$this->Session->setFlash('Category has been saved successfully.', 'default', 'success');
			$this->redirect($this->referer());
		}
	}

	protected function _existsParentId($id = null) {
		
		$parentIdExists = $this->Category->find('first', array(
				'conditions' => array(
					'Category.id' => $id,
					'Category.is_deleted' => (int)false,
					'Category.is_activated' => (int)true

				)
		));
		
		if (empty($parentIdExists)) {
			return false;
		}

		return true;
	}

	public function admin_getCategoryData($categoryId) {
		$this->request->allowMethod('ajax');
		if (!empty($categoryId)){ 
			$categoryId = base64_decode($categoryId);
			$data = $this->Category->find('first', array(
				'conditions' => array(
					'Category.id' => $categoryId
				),
				'fields' => array(
					'title',
					'id',
					'parent_id'
				)
			));
			
			if (empty($data)) {
				throw new Exception("Some error occured. Please try again !");
			}

			$this->set(array('resp' => $data, '_serialize' => 'resp'));
		}

	}

	public function admin_edit() {
		$this->request->allowMethod('post');
		$data = $this->request->data;

		if (!empty($data)) {
			$category = $this->Category->findById($this->request->data['Category']['id'],array('id'));
			
			if (empty($category)) {
				$this->Session->setFlash('Some error occured, please try again!', 'default', 'error');
				$this->redirect($this->referer());
			}

			$categoryData['Category'] = array(
				'parent_id' => null,
				'title' => $data['Category']['title'],
				'id' => $category['Category']['id']
			);

			if (!$this->Category->save($categoryData)) {
				$errors = $this->Category->validationErrors;
				$errorMsg = $this->_setSaveAssociateValidationError($errors);
				$this->Session->setFlash('Category add request not completed due to following errors: <br/>' . $errorMsg . ' Try again!', 'default', 'error');
				$this->redirect($this->referer());
			}

			$this->Session->setFlash('Category has been saved successfully.', 'default', 'success');
			$this->redirect($this->referer());
		}
	}

}
