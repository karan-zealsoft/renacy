<?php
// App::uses('User', 'View/Helper'); 
class HomesController extends AppController
{
	public $helper = array('Html', 'Form', 'Js', 'Admin', 'UserData');
	public $components = array('RequestHandler', 'Paginator');
	public $uses = array();
	public $layout = "";

	public function beforeFilter() {
		parent::beforeFilter();
		
	}

/**
 * Method index for home page
 *
 * @return void 
 */
	public function index() {
			
	}
}
