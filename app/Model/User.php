<?php
App::uses('AppModel', 'Model');
App::uses('CakeSession', 'Model/Datasource');
App::uses('AuthComponent', 'Controller/Component');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');


class User extends AppModel {
	public $name = 'User';
   	public $actsAs = array('Containable');
	public $validate = array(
		'phone' => array(
			'rule1' => array(
				'rule' => '/^[a-zA-Z0-9 -()+]+/i',
				'allowEmpty' => true,
				'message' => 'Please enter a valid phone no.'
			),
			'maxLength' => array(
				'rule' => array('maxLength', '50'),
				'message' => 'Please enter no more than 50 characters.'
			)
		),
		'first_name' => array(
			'rule1' => array(
				'rule' => 'notBlank',
				'message' => 'Please enter first name.'
			),
			'minLength' => array(
				'rule' => array('minLength', '3'),
				'message' => 'First name must be at least 3 characters long.'
			),
			'maxLength' => array(
				'rule' => array('maxLength', '50'),
				'message' => 'First name cannot more than 50 characters long.'
			)
		),
		'last_name' => array(
			'rule1' => array(
				'rule' => 'notBlank',
				'message' => 'Please enter last name.'
			),
			'maxLength' => array(
				'rule' => array('maxLength', '50'),
				'message' => 'Last name cannot more than 50 characters long.'
			),
			'minLength' => array(
				'rule' => array('minLength', '3'),
				'message' => 'Last name must be at least 3 characters long.'
			)
		),
		'username' => array(
			'rule1' => array(
				'rule' => 'notBlank',
				'message' => 'Please enter user name.'
			),
			'isUnique' => array(
				'rule' => 'isUnique',
				'message' => 'Please choose another username, given username already exists.',
				'on' => 'create'
			),
			'minLength' => array(
				'rule' => array('minLength', '3'),
				'message' => 'Username must be at least 3 characters long.'
			),
			'maxLength' => array(
				'rule' => array('maxLength', '50'),
				'message' => 'Username cannot more than 50 characters long.'
			)
		),
		'password' => array(
			'rule1' => array(
				'rule' => 'notBlank',
				'required' => true,
				'message' => 'Please enter password'
			),
			'rule2' => array(
				'rule' => array('minLength', 8),
				'message' => 'Password must be at least 8 characters long'
			),
			'rule3' => array(
				'rule' => array('maxLength', 50),
				'message' => 'Password cannot more than 50 characters long'
			)
		),
		'confirm_password' => array(
			'rule1' => array(
				'rule' => 'notBlank',
				'required' => true,
				'message' => 'Please enter confirm password.'
			),
			'rule2' => array(
				'rule' => 'confirmPassword',
				'message' => 'Confirm password should be same as password.'
			),
			'rule3' => array(
				'rule' => array('maxLength', 50),
				'message' => 'Confirm Password cannot more than 50 characters long'
			)
		),
		'email' => array(
			'rule1' => array(
				'rule' => 'notBlank',
				'message' => 'Please enter email address.'
			),
			'rule2' => array(
				'rule' => 'email',
				'message' => 'Please enter valid email address.'
			),
			'isUnique' => array(
				'rule' => 'isUnique',
				'message' => 'This email is already in use.',
				'on' => 'create'
			)
		)
	);

/**
 * Custom validator function to confirm password
 *
 * @return bool
 */
	public function confirmPassword() {
		$valid = false;
		if ($this->data['User']['password'] == $this->data['User']['confirm_password']) {
			$valid = true;
		}
		return $valid;
	}

/**
 * Callback method that runs before save opeation on
 * User Model. Place any pre-save logic in this function
 *
 * @param array $options = array() Options
 * @return bool
 */
	public function beforeSave($options = array()) {
		if (!empty($this->data[$this->alias]['password'])) {
			$passwordHasher = new SimplePasswordHasher(array('hashType' => 'sha1'));
			$this->data[$this->alias]['password'] = $passwordHasher->hash(
				$this->data[$this->alias]['password']
			);
		}
		return true;
	}

	public function activeUserCount() {
		$activeUsers = $this->find('count',array(
			'conditions' => array(
					'User.user_type_id' => configure::read('UserTypes.User'),
					'User.is_activated' => 1,
					'User.is_deleted' => 0
				),
			'recursive' => -1
		));
		return $activeUsers;
	}
}