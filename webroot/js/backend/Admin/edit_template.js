$(function () {
	var editEmailTemplateValidate = function(ele){
		$(ele).validate({
            rules: {
                "data[EmailTemplate][template_used_for]": {
                    required: true
                },
                "data[EmailTemplate][subject]": {
                    required: true
                }
                ,
                "data[EmailTemplate][mail_body]": {
                    required: true
                }
            }
        });
	};
	$(document).ready(function(){
		editEmailTemplateValidate('#EditEmailTemplate');
	});

});
