$(function () {
      var viewUserData = function(url) {
        $.ajax({
            url: url,
            success:function( data ) {
                var popupTemplate = $("#viewUserField").html();
                resultData = _.template(popupTemplate, {data: data});
                $('#userDataContainer').html(resultData);
            }
        });
    };

    var addFrontUserValidate = function(ele){
        $(ele).validate({
            rules: {
                "data[User][username]": {
                    required: true,
                    'minlength': 3
                },
                "data[User][email]": {
                    required: true,
                    email: true
                },
                "data[User][password]": {
                    required: true,
                    minlength: 6
                },
                "data[User][confirm_password]": {
                    required: false,
                    equalTo: '#user-new-password'
                },
                "data[User][first_name]": {
                    required: true,
                    alphaberspecialcharsonly: true,
                    minlength: 3
                },
                "data[User][last_name]": {
                    required: true,
                    alphaberspecialcharsonly: true,
                     minlength: 3
                },
                "data[UserProfile][mobile]": {
                    required: true,
                    number: true
                }
            },
            messages: {
                "data[User][first_name]": {
                    alphaberspecialcharsonly: 'Invalid characters in First Name'
                },
                "data[User][last_name]": {
                    alphaberspecialcharsonly: 'Invalid characters in Last Name'
                }
            }
        });
    };

	$(document).ready(function(){
        $(document).on('click', '.view_user_data', function(){
            var url = $(this).attr('data-url');
            viewUserData(url);
        });
        $(document).on('click', '.editUser', function(){
            $('#view-user').find('input[type=text]').each(function() {
                $(this).prop('readonly',false);
            })
        });
        addFrontUserValidate('#add-user');
    });
});