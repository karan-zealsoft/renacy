$(function () {
    var addSubAdminValidate = function(ele){
		$(ele).validate({
			rules: {
                "data[User][username]": {
                    required: true,
                    minlength: 3
                },
				"data[User][email]": {
                    required: true,
                    email: true,
                    
                },
                "data[User][password]": {
                    required: true,
                    minlength: 6
                },
                "data[User][confirm_password]": {
                    required: true,
                    equalTo: '#subadmin-new-password'
                },
                "data[User][first_name]": {
                    required: true,
                    alphaberspecialcharsonly: true,
                    minlength: 3
                },
                "data[User][last_name]": {
                    required: true,
                    alphaberspecialcharsonly: true,
                    minlength: 3
                },
               
			},
            messages: {
                "data[User][first_name]": {
                    alphaberspecialcharsonly: 'Invalid characters in First Name'
                },
                "data[User][last_name]": {
                    alphaberspecialcharsonly: 'Invalid characters in Last Name'
                }
            }
		});
	};
	$(document).ready(function(){
		addSubAdminValidate('#add-sub-admin');
	});

});
