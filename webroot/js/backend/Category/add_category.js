//"use strict";
$(function () {
    var addCategoryValidate = function(ele){
        $(ele).validate({
			rules: {
                "data[Category][title]": {
                    required: true,
                    minlength: 3
                }
			},
            messages: {
                
            }
		});
	};

    var editCategory = function(url) {
        $.ajax({
            url: url,
            dataType: "json",
            success:function( data ) {
                var popupTemplate = $("#editCategoryeData").html();
                    resultData = _.template(popupTemplate, {data: data});
                    $('#categoryDataContainer').html(resultData);
            },
            error:function(error) {
                $('#flashMessage').html('Some error occured, please try again!');
                $('.alert')
                            .addClass('alert-danger')
                            .show();
            }
        });
    };

	$(document).ready(function(){
        addCategoryValidate('#add-category');
        addCategoryValidate('#edit-category');
        $(document).on('click', '.edit_category_data', function(){
            var url = $(this).attr('data-url');
            editCategory(url);
        });
	});

});
