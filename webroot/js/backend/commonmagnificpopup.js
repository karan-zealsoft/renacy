$(function () {

  // Inline popups
  var magniPopups = function(ele){
      $(ele).magnificPopup({
        delegate: 'a.magniPopup',
          removalDelay: 800, //delay removal by X to allow out-animation
          callbacks: {
            beforeOpen: function() {
                this.st.mainClass = this.st.el.attr('data-effect');
            }
          },
        midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
      });
  };

  $(document).ready(function(){
    magniPopups('#inline-popups');
  });
});
$(document).on('click', '.deleteModalButton', function(){
      var url = $(this).attr('data-url');
      $('#sureDelete').attr('href',url);
    });

$(document).on('click', '.activeinactivemodalbutton', function(){
  var url = $(this).attr('data-url');
  $('.sureActiveInactive').attr('href',url);
});

/*$(document).on('click', '.makeusertrusted', function(){
  var url = $(this).attr('data-url');
  $('.usertrustedsure').attr('href',url);
});*/

$(document).on('click', '.close-btn', function(){
  $.magnificPopup.close();
});