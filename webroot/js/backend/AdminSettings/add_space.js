$(function () {
	var editSpaceData = function(url) {
        $.ajax({
            url: url,
            success:function( data ) {
        		var popupTemplate = $("#editSpaceField").html();
				resultData = _.template(popupTemplate, {data: data});
                $('#spaceDataContainer').html(resultData);
            }
        });
    };
    var addSpaceValidate = function(ele){
		$(ele).validate({
			rules: {
                "data[Space][name]": {
                    required: true
                }
			}
		});
	};
	$(document).ready(function(){
		$(document).on('click', '.edit_space_data', function(){
            var url = $(this).attr('data-url');
            editSpaceData(url);
        });
		addSpaceValidate('#add-space');
		addSpaceValidate('#edit-space');
	});

});
