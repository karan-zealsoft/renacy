$(function () {
	var editCmsValidate = function(ele){
		$(ele).validate({
            rules: {
                "data[CmsPage][page_title]": {
                    required: true
                },
                "data[CmsPage][page_name]": {
                    required: true
                },
                "data[CmsPage][meta_title]": {
                    required: true
                },
                "data[CmsPage][meta_keyword]": {
                    required: true
                }
            }
        });
	};
	$(document).ready(function(){
		editCmsValidate('#EditCms');
	});

});
