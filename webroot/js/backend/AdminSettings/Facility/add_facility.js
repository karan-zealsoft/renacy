$(function () {
	var editFacilityData = function(url) {
        $.ajax({
            url: url,
            success:function( data ) {
        		var popupTemplate = $("#editFacilityField").html();
				resultData = _.template(popupTemplate, {data: data});
                $('#facilityDataContainer').html(resultData);
            }
        });
    };
    var addFacilityValidate = function(ele){
		$(ele).validate({
			rules: {
                "data[Facility][name]": {
                    required: true
                },
                // "data[Facility][icon]": {
                //     required: false,
                //     fileType: true
                // }
            }
        });
        // jQuery.validator.addMethod("fileType", function() {
        //     var ext = $('.upload-icon').val().split('.').pop().toLowerCase();
        //     if(empty(ext)) {
        //         var ext = $('.edit-upload-icon').val().split('.').pop().toLowerCase();
        //     }
        //     if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
        //         return false;
        //     }
        //     return true;
        // }, "File Type not supported.");
    };

	$(document).ready(function(){
		$(document).on('click', '.edit_facility_data', function(){
            var url = $(this).attr('data-url');
            editFacilityData(url);
        });
		addFacilityValidate('#add-facility');
		addFacilityValidate('#edit-facility');
	});

});
