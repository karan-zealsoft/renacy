$(function () {
	var editPropertyData = function(url) {
        $.ajax({
            url: url,
            success:function( data ) {
        		var popupTemplate = $("#editPropertyField").html();
				resultData = _.template(popupTemplate, {data: data});
                $('#propertyDataContainer').html(resultData);
            }
        });
    };
    var addPropertyValidate = function(ele){
		$(ele).validate({
			rules: {
                "data[Property][name]": {
                    required: true
                }
			}
		});
	};
	$(document).ready(function(){
		$(document).on('click', '.edit_property_data', function(){
            var url = $(this).attr('data-url');
            editPropertyData(url);
        });
		addPropertyValidate('#add-property');
		addPropertyValidate('#edit-property');
	});

});
