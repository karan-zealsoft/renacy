$(function () {
	var CouponAddValidate = function(ele){
		$(ele).validate({
			rules: {
				"data[DiscountCoupon][name]": {
					required: true
                },
                "data[DiscountCoupon][description]": {
					required: true
                },
                "data[DiscountCoupon][code]": {
					required: true,
					minlength: 6,
                    alphanumeric: true
                },
                "data[DiscountCoupon][discount_type]": {
                    required: true,
                },
                "data[DiscountCoupon][for_user_type]": {
                    required: true,
                },
                "data[DiscountCoupon][flat_amount]": {
                    number: true,
                    checkRequired: true
                },
                "data[DiscountCoupon][discount_percentage]": {
					//required: true,
                    max: 100,
                    min: 1,
                    number: true,
                    checkRequired: true
                },
                "data[DiscountCoupon][max_amount_discount]": {
					// required: true,
                    /*greaterThan: '#discount-amount', */
                    // min: 1,
                    number: true
                }
			},
            messages: {
                "data[DiscountCoupon][discount_percentage]": {
                    max: 'Only numbers are allowed to be entered in this field and value should be less than and equal to 100.'
                }
            },
            groups: {
                priceGroup: 'data[DiscountCoupon][discount_percentage] data[DiscountCoupon][flat_amount]',
            },
        });
        jQuery.validator.addMethod("checkRequired", function() {
            if ($('#add-discount-coupon input[name="data[DiscountCoupon][flat_amount]"]').val() == 0 &&
                    $('#add-discount-coupon input[name="data[DiscountCoupon][discount_percentage]"]').val() == 0
                    ) {
                return false;
            }
            return true;

        }, "Please enter discount.");
	};

    var CouponEditValidate = function(ele){
        $(ele).validate({
            rules: {
                "data[DiscountCoupon][name]": {
                    required: true
                },
                "data[DiscountCoupon][description]": {
                    required: true
                },
                "data[DiscountCoupon][code]": {
                    required: true,
                    minlength: 6,
                    alphanumeric: true
                },
                "data[DiscountCoupon][discount_type]": {
                    required: true,
                },
                "data[DiscountCoupon][for_user_type]": {
                    required: true,
                },
                "data[DiscountCoupon][flat_amount]": {
                    number: true,
                    checkRequired: true
                },
                "data[DiscountCoupon][discount_percentage]": {
                    // required: true,
                    max: 100,
                    min: 1,
                    number: true,
                    checkRequired: true
                },
                "data[DiscountCoupon][max_amount_discount]": {
                    // required: true,
                    /*greaterThan: '#discount-amount-edit', */
                    min: 1,
                    number: true
                }
            },
            messages: {
                "data[DiscountCoupon][flat_amount]": {
                    required: "Enter Price.",
                },
                "data[DiscountCoupon][discount_percentage]": {
                    required: "Enter Price.",
                    max: 'Only numbers are allowed to be entered in this field and value should be less than and equal to 100.'
                }
            },
            groups: {
                priceGroup: 'data[DiscountCoupon][discount_percentage] data[DiscountCoupon][flat_amount]',
            },
        });
        jQuery.validator.addMethod("checkRequired", function() {
            if ($('#edit-discountcoupon input[name="data[DiscountCoupon][flat_amount]"]').val() == 0 &&
                    $('#edit-discountcoupon input[name="data[DiscountCoupon][discount_percentage]"]').val() == 0
                    ) {
                return false;
            }
            return true;

        }, "Please enter discount.");
    };

	// Edit
    var editCouponData = function(url) {
        $.ajax({
            url: url,
            success:function( data ) {
        		var popupTemplate = $("#editDiscountCouponField").html();
				resultData = _.template(popupTemplate, {data: data});
                $('#discountCouponDataContainer').html(resultData);
            }
        });
    };

    var disableCouponFront = function(url) {
        $.ajax({
            url: url,
            type: 'POST',
            dataType: "json",
            // success:function( data ) {
            //     if(data.status == 'error'){
            //             $(current_this).attr('checked', false);
            //             $('#showfront'+data.frontendsetcoupon).prop("checked", true);
            //         }
            // }
            complete: function(){
                location.reload(true);
            }
        });
    };

	$(document).ready(function(){
        $(document).on('click', '.edit_discountcoupon_data', function(){
            var url = $(this).attr('data-url');
            editCouponData(url);
        });

        $(document).on('click', '#disable-discount-frontend', function(){
            var url = $(this).attr('data-url');
            disableCouponFront(url);
        });

        CouponAddValidate('#add-discount-coupon');
		CouponEditValidate('#edit-discountcoupon');

        $('#add-discount-coupon').submit(function(e){
            if ($('#discount-amount').val() > 20 && $(this).valid()) {
                if (!confirm('Are you sure you want to set the percentage amount above than 20%?')) {
                    e.preventDefault(); //prevent from to submit
                }
            }
        });

        $(document).on('click', '.is-frontend', function(){
            var coupon_id = $(this).val();
            var url = $('.discount-path').attr("data-discountUrl");
            var current_this = this;

            var request ={'coupon_id': coupon_id};
            $.ajax({
                url: url,
                type: 'POST',
                dataType: "json",
                data: {result: request},
                success:function(data) {
                    if(data.status == 'error'){
                        $(current_this).attr('checked', false);
                        $('#showfront'+data.frontendsetcoupon).prop("checked", true);
                    }
                    alert(data.msg);
                }
            });
        });

        $('#add-discount-coupon input[type=radio]').change(function() {       
            if (this.value == 0) {
                $('.flat-discount').show();
                $('.discount-per').hide();
                $('.discount-per').find('input').val('');
            } else {
                $('.flat-discount').hide();
                $('.flat-discount').find('input').val('');

                $('.discount-per').show();

            }
        });

        $('#addDiscountCoupon').on('shown.bs.modal', function () {
            if($("#add-discount-coupon input[name='data[DiscountCoupon][discount_type]']").val() == 0) {
                $('.flat-discount').show();
                $('.discount-per').hide();
                $('.discount-per').find('input').val('');
            } else {
                $('.flat-discount').hide();
                $('.flat-discount').find('input').val('');

                $('.discount-per').show();
            }
        });

        $('#editDiscountCouponModal').on('shown.bs.modal', function () {
            if($("#edit-discountcoupon input[name='data[DiscountCoupon][discount_type]']:checked").val() == 0) {
                $('.edit-flat-discount').show();
                $('.edit-discount-per').hide();
            } else {
                $('.edit-flat-discount').hide();
                $('.edit-discount-per').show();
            }
        });

        $('#edit-discountcoupon input[type=radio]').change(function() {
            if (this.value == 0) {
                $('.edit-flat-discount').show();
                $('.edit-discount-per').hide();
            } else {
                $('.edit-flat-discount').hide();
                $('.edit-discount-per').show();

            }
        });
	});
});
