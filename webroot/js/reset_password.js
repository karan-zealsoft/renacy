$(function () {
	var resetPasswordValidate = function(ele){
		$(ele).validate({
			rules: {
                "data[User][password]": {
                    required: true,
                    minlength: 6
                },
                "data[User][confirm_password]": {
                    equalTo: '#new-password'
                }
			}
		});
	};
	$(document).ready(function(){
		resetPasswordValidate('#reset-password-validate');
	});

});
