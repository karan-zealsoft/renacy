$(function () {
	var changePasswordValidate = function(ele){
		$(ele).validate({
			rules: {
				"data[User][old_password]": {
                    required: true
                },
                "data[User][password]": {
                    required: true,
                    minlength: 6
                },
                "data[User][confirm_password]": {
                    equalTo: '#new-password'
                }
			}
		});
	};
	$(document).ready(function(){
		changePasswordValidate('#change-password');
	});

});
